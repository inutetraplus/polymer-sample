import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

class CustomButton extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        button {
          font-weight: 400;
          line-height: 1.25;
          border: 1px solid transparent;
          padding: .5rem 1rem;
          font-size: 1rem;
          border-radius: .25rem;
          color: #fff;
          background-color: var(--app-primary-color);
          transition: all .2s ease-in-out;
        }
        :host([clicked]) button {
          background-color: var(--dark-accent-color);
        }
        button:hover {
          background-color: #fff;
          color: var(--app-primary-color);
          border-color: var(--app-primary-color);
        }
        :host([clicked]) button:hover {
          color: var(--dark-accent-color);
          border-color: var(--dark-accent-color);
        }
      </style>
      <button on-click="toggle" type="button" name="button">
        <slot name="value"></slot>
      </button>
    `;
  }
  static get properties () {
    return {
      clicked: {
        type: Boolean,
        value: false,
        notify: true,
        reflectToAttribute: true
      }
    };
  }
  constructor() {
    super();
  }
  toggle() {
    this.clicked = !this.clicked;
  }
}

customElements.define('custom-button', CustomButton);
